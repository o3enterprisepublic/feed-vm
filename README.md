# FEED VM

FEED è un router Dicom, che riceve uno store e può eseguire forward tramite STOW_RS o DICOM. Il sistema non permette di fare modifiche anagrafiche e riconciliazioni.

Per installare la  VM con il sistema già installato bisogna:
* installare VirtualBox di Oracle
* scaricare la VM dal seguente link [Download VM Feed](https://drive.google.com/open?id=17qTqynJyESklPJf8nI4_kdcSLJEYA8-O)
* da VirtualBox andare su file->importa applicazione virtuale...->scegliere il file appena scaricato->Successivo->Importa
* fare lo start della VM
* controllare l'ip della macchina (accedendo con root/root e lanciando il comando "ip add")
* da un browser collegarsi all'interfaccia web del feed (ip controllato con il comando precedente sulla porta 9191)


Prima configurazione:
Appena collegati per la prima volta all'interfaccia web del feed sarà presente la seguente pagina

![datei](images/ConfigurationPageFeed.JPG)

In questa pagina è possibile configurare l'aetitle dicom del sistema FEED (mettere un valore diverso per ogni installazione), i tre campi per le folder lasciarli come i default.
Inoltre è possibile configurare le porte (DICOM/WORKLIST e HTTP), se non ci sono particolari esigenze lasciare quelle di default e ricordarsele.
Ultima configurazione è l'url del server cloud che riceverà gli esami.

Una volta configurati e salvati i parametri bisogna collegarsi nuovamente alla macchina e restartare il sistema.
I comandi da dare una volta collegati sono:
* cd /var/docker_compose_files/feed
* docker-compose restart feed

Se è stato fatto tutto correttamente accedendo nuovamente all'ip del feed sulla porta http configurata si visualizzerà la seguente pagina.

![datei](images/HomePageFeed.JPG)